package main

import(
	"github.com/gorilla/mux"
	"net/http"
	"log"
	"flag"
	"awesomeProject/router"
)
var (
	hostname string
	port string
	static string
)

func init() {
	flag.StringVar(&hostname, "hostname", "0.0.0.0", "The hostname or IP on which the REST server will listen")
	flag.StringVar(&port, "port", "8000", "The port on which the REST server will listen")
	flag.StringVar(&static, "static", "./static", "静态资源路径")
}

func main() {
	flag.Parse()

	r := mux.NewRouter()

	r.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir(static))))

	r.HandleFunc( "/", func(w http.ResponseWriter, r *http.Request)  {
		w.Write([]byte("Gorilla!\n"))
	})

	r.HandleFunc("/upload", router.UploadPage).Methods("get")
	r.HandleFunc("/upload/{category}", router.UploadPage).Methods("get")

	r.HandleFunc("/upload/{category}", router.Upload).Methods("post")
	r.HandleFunc("/show/{category}", router.Show).Methods("get")

	// Bind to a port and pass our router in
	log.Fatal(http.ListenAndServe(":" + port, r))
}
